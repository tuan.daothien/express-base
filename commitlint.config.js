module.exports = {
  extends: ['@commitlint/config-conventional'],
  rules: {
    'subject-full-stop': [2, 'always', '.'],
    'type-case': [2, 'always', 'snake-case'],
    'scope-case': [2, 'always', 'snake-case'],
    'subject-case': [2, 'always', 'sentence-case']
  }
};
