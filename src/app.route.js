'use strict';

const express = require('express');
const validate = require('./middlewares/validators/validator');
const { authenticateJwt } = require('./middlewares/auth.middleware');
const router = express.Router();

router.get('/', (req, res) => res.send('This is user-service'));

const accountController = require('./controllers/account.controller');
const accountValidator = require('./middlewares/validators/account.validator');
router.post(
  '/register',
  validate(accountValidator.validateRegister()),
  accountController.register
);
router.get('/:id/profile', accountController.getProfile);
router.put('/profile', authenticateJwt, accountController.updateProfile);
router.post('/:id/social', authenticateJwt, accountController.socialize);
router.get('/:id/followers', accountController.getFollowers);
router.get('/orders', authenticateJwt, accountController.getPersonalOrders);

const multipart = require('./middlewares/multipart.middleware');
router.post(
  '/:id/avatar',
  multipart.parse([
    {
      name: 'avatar',
      validations: { maxSize: { value: 95000 } }
    }
  ]),
  accountController.upload
);

router.post(
  '/login',
  validate(accountValidator.validateLogin()),
  accountController.login
);

module.exports = router;
