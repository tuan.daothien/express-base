'use strict';

const express = require('express');
const bodyParser = require('body-parser');
const LogService = require('./services/log.service');
const AppConfig = require('./app.config');
const { handleError, AppError } = require('./utils/helpers/errors.helper');

const app = express();
const port = AppConfig.APP_PORT;

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static(__dirname + '/static'));

app.use('/api/users', require('./app.route'));
app.use('', require('./app.route'));
app.use((instance, req, res, next) => {
  let error;
  let errorMsg = '';
  let issuer = '';
  if (instance instanceof AppError) {
    error = instance;
    issuer = 'APP';
    errorMsg = instance.message;
  } else {
    error = instance.error;
    issuer = instance.issuer;
    if (error instanceof AppError) {
      errorMsg = error.message;
    } else {
      errorMsg = error ? error.toString() : 'Unknown server error';
    }
  }
  LogService.error(issuer, errorMsg);
  handleError(error, res);
});

app.listen(port, () => {
  LogService.info('APP', 'App listening on port', port);
});
