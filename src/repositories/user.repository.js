'use strict';

const BaseRepository = require('../base/base.repository');
const { User } = require('../sequelize/models');

class UserRepository extends BaseRepository {
  async findByUsername(username) {
    const user = await User.findOne({
      where: { username }
    });
    return user;
  }

  async createUser(username, password) {
    const result = await User.create({
      username,
      password
    });
    return result;
  }
}

module.exports = new UserRepository(User);
