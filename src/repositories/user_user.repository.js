'use strict';

const BaseRepository = require('../base/base.repository');
const { UserUser } = require('../sequelize/models');

class UserUserRepository extends BaseRepository {
  async find(actorId, targetId, action) {
    const record = await UserUser.findOne({
      where: {
        actorId,
        targetId,
        action
      }
    });
    return record;
  }

  async findFollow(actorId, targetId) {
    return this.find(actorId, targetId, 'follow');
  }

  async findOrCreateFollow(actorId, targetId) {
    const [data] = await UserUser.findOrCreate({
      where: { actorId, targetId, action: 'follow' }
    });

    return data;
  }
}

module.exports = new UserUserRepository(UserUser);
