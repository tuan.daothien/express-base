'use strict';

const { Model, DataTypes } = require('sequelize');
const bcrypt = require('bcrypt');

module.exports = (sequelize) => {
  class User extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      User.belongsToMany(models.User, {
        through: models.UserUser,
        as: {
          singular: 'actor',
          plural: 'actors'
        },
        foreignKey: { name: 'actorId' }
      });
      User.belongsToMany(models.User, {
        through: models.UserUser,
        as: {
          singular: 'target',
          plural: 'targets'
        },
        foreignKey: { name: 'targetId' }
      });
    }

    static hasAttribute(attribute) {
      return User.rawAttributes[attribute] !== undefined;
    }
  }
  User.init(
    {
      username: {
        type: DataTypes.STRING,
        allowNull: false
      },
      password: {
        type: DataTypes.STRING,
        allowNull: false
      },
      rank: {
        type: DataTypes.INTEGER,
        allowNull: false,
        defaultValue: 0
      }
    },
    {
      sequelize,
      tableName: 'user',
      underscored: true,
      hooks: {
        beforeCreate: (user) => {
          user.password = bcrypt.hashSync(user.password, 12);
        },
        beforeUpdate: (user) => {
          if (user.changed('password')) {
            if (
              bcrypt.compareSync(
                user.dataValues.password,
                user._previousDataValues.password
              )
            ) {
              user.password = user._previousDataValues.password;
            } else {
              user.password = bcrypt.hashSync(user.dataValues.password, 12);
            }
          }
        }
      }
    }
  );
  return User;
};
