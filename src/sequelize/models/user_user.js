'use strict';

const { Model, DataTypes } = require('sequelize');

module.exports = (sequelize) => {
  class UserUser extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // UserUser.belongsTo(models.User, { as: 'actor', onDelete: 'CASCADE' });
      // UserUser.belongsTo(models.User, { as: 'target', onDelete: 'CASCADE' });
    }
  }
  UserUser.init(
    {
      actorId: {
        type: DataTypes.INTEGER,
        allowNull: false,
        references: {
          model: 'User',
          key: 'id'
        }
      },
      targetId: {
        type: DataTypes.INTEGER,
        allowNull: false,
        references: {
          model: 'User',
          key: 'id'
        }
      },
      action: {
        type: DataTypes.STRING,
        allowNull: false
      }
    },
    {
      sequelize,
      tableName: 'user_user',
      underscored: true
    }
  );
  return UserUser;
};
