'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert('user', [
      {
        username: 'tuan.dao',
        password:
          '$2b$10$1S2..v1kmWWMkRtb3cqF4uXRX6I9NONDYPEaBe8R0dnERABSDJmhy',
        rank: 1,
        created_at: new Date(),
        updated_at: new Date()
      },
      {
        username: 'lap.top',
        password:
          '$2b$10$Z5dfXEUUqup0pGLG/FhgHeVfMhVkOOkzJVCM0yGG9q95Im9bFQabu',
        rank: 2,
        created_at: new Date(),
        updated_at: new Date()
      },
      {
        username: 'com.hop',
        password:
          '$2b$10$fduv8AMYSGWrh.nf37koRu1dRdEDUyeS8i0ujBlQbuXiIoNUNrfqi',
        rank: 3,
        created_at: new Date(),
        updated_at: new Date()
      }
    ]);

    return await queryInterface.bulkInsert('user_user', [
      {
        actor_id: 1,
        target_id: 2,
        action: 'follow',
        created_at: new Date(),
        updated_at: new Date()
      },
      {
        actor_id: 1,
        target_id: 3,
        action: 'follow',
        created_at: new Date(),
        updated_at: new Date()
      },
      {
        actor_id: 3,
        target_id: 2,
        action: 'follow',
        created_at: new Date(),
        updated_at: new Date()
      }
    ]);
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('user_user', null, {});
    return queryInterface.bulkDelete('user', null, {});
  }
};
