'use strict';

const path = require('path');
const fs = require('fs');
const AppConfig = require('../app.config');
const BaseClass = require('../base/base.class');

const LOG_TYPE = {
  INFO: 'INFO',
  ERROR: 'ERROR'
};
const LOGS_DIR = path.join(process.cwd(), 'archive', 'logs');
const makeSureLogsDir = (dir) => {
  if (!fs.existsSync(dir)) {
    return fs.mkdirSync(dir, { recursive: true });
  }
};

class LogService extends BaseClass {
  constructor(issuer) {
    super();
    this.issuer = issuer;
  }

  static info(issuer, message, ...extras) {
    const log = LogService.genLogString(
      LOG_TYPE.INFO,
      issuer,
      message,
      ...extras
    );
    console.log(log);
  }

  static error(issuer, message, ...extras) {
    const log = LogService.genLogString(
      LOG_TYPE.ERROR,
      issuer,
      message,
      ...extras
    );
    console.error(log);
  }

  info(message, ...extras) {
    LogService.info(this.issuer, message, ...extras);
  }

  error(message, ...extras) {
    LogService.error(this.issuer, message, ...extras);
  }

  static logSave(issuer, logType, message, ...extras) {
    makeSureLogsDir(LOGS_DIR);
    const stream = fs.createWriteStream(path.join(LOGS_DIR, 'errors.log'), {
      flags: 'a'
    });
    stream.write(
      LogService.genLogString(logType, issuer, message, ...extras) + '\n'
    );
  }

  static logSaveInfo(issuer, message, ...extras) {
    LogService.logSave(issuer, LOG_TYPE.INFO, message, ...extras);
  }

  static logSaveError(issuer, message, ...extras) {
    LogService.logSave(issuer, LOG_TYPE.ERROR, message, ...extras);
  }

  logSave(logType, message, ...extras) {
    LogService.logSave(this.issuer, logType, message, ...extras);
  }

  logSaveInfo(message, ...extras) {
    LogService.logSaveInfo(message, ...extras);
  }

  logSaveError(message, ...extras) {
    LogService.logSaveError(message, ...extras);
  }

  static genLogString(logType, issuer, message, ...extras) {
    const time = new Date().toISOString();
    const log = [message, ...extras].join(' ');
    return [
      time,
      `[${logType}]`,
      `[${AppConfig.APP_NAME}]`,
      `[${issuer}]`,
      ':',
      log
    ].join(' ');
  }
}

module.exports = LogService;
