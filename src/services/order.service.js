'use strict';

const BaseHttpService = require('../base/base.http.service');

class OrderService extends BaseHttpService {
  async getPersonalOrders(userId, auth) {
    return await this.get(
      `/${userId}/personal`,
      this.buildAuthHeaderOnly(auth)
    ).then((response) => response.data);
  }
}

module.exports = new OrderService('order');
