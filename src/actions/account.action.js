'use strict';

const { Op } = require('sequelize');
const path = require('path');
const fs = require('fs');
const crypto = require('crypto');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const { User } = require('../sequelize/models');
const UserRepository = require('../repositories/user.repository');
const UserUserRepository = require('../repositories/user_user.repository');
const AppConfig = require('../app.config');
const OrderService = require('../services/order.service');
const Validator = require('../utils/helpers/validator.helper');
const BaseClass = require('../base/base.class');

class AccountAction extends BaseClass {
  async register(username, password) {
    const exist = await UserRepository.findByUsername(username);
    if (exist) {
      return [1, null];
    }

    const result = await UserRepository.createUser(username, password);
    return [0, result];
  }

  async getProfile(id) {
    const result = await UserRepository.findById(id);
    return result;
  }

  async updateProfile(id, data) {
    const result = await UserRepository.updateById(id, data);
    return result;
  }

  async socialize(id, actorId, action) {
    let result = null;
    switch (action) {
      case 'follow': {
        const actor = await UserRepository.findById(actorId);
        const target = await UserRepository.findById(id);
        if (!actor || !target) {
          return null;
        }

        result = await UserUserRepository.findOrCreateFollow(actorId, id);
        break;
      }
      case 'unfollow': {
        const relation = await UserUserRepository.findFollow(actorId, id);

        if (relation) {
          await relation.destroy();
        }
        result = await UserRepository.findById(id);
        break;
      }
      default: {
        result = null;
      }
    }

    return result;
  }

  async getFollowers(id, options) {
    const searchType = Op.and;
    const relative = ['username'];
    const query = {
      attributes: ['id', 'username', 'rank'],
      where: { [searchType]: [] },
      include: [
        {
          attributes: [],
          model: User,
          as: 'actors',
          required: true,
          through: {
            attributes: [],
            where: { targetId: id, action: 'follow' }
          }
        }
      ]
    };
    for (const key in options) {
      if (
        Validator.isNullOrUndefined(options[key]) ||
        options[key] === '' ||
        !User.hasAttribute(key)
      ) {
        continue;
      }

      let fieldOption = null;
      if (relative.includes(key)) {
        fieldOption = {
          [key]: { [Op.iLike]: `%${options[key]}%` }
        };
      } else {
        fieldOption = { [key]: options[key] };
      }
      query.where[searchType].push(fieldOption);
    }
    if (!query.where[searchType].length) {
      delete query.where[searchType];
    }
    const followers = await User.findAll(query);

    return followers;
  }

  async upload(id, files) {
    const fileProcesses = Object.keys(files).reduce((fileProcesses, key) => {
      files[key].forEach((file) => {
        const fileName =
          file.fieldname +
          '-' +
          Date.now() +
          '-' +
          crypto.randomBytes(10).toString('hex') +
          path.extname(file.originalname);

        const stream = fs.createWriteStream(path.join('uploads', fileName));
        fileProcesses.push(
          new Promise((resolve, reject) => {
            stream.on('error', (err) => {
              reject(err);
            });
            stream.on('finish', () => resolve(true));
            stream.write(file.buffer);
            stream.end();
          })
        );
      });
      return fileProcesses;
    }, []);

    await Promise.all(fileProcesses);
    return true;
  }

  async login(username, password) {
    let author = 0;
    const user = await UserRepository.findByUsername(username);

    if (user) {
      const passwordCheck = await bcrypt.compare(password, user.password);
      author = passwordCheck ? 1 : 0;
    }

    if (!author) {
      return null;
    }
    const accessToken = jwt.sign(
      { id: user.id, username: user.username },
      AppConfig.APP_JWT_SECRET
    );
    return accessToken;
  }

  async getPersonalOrders(id, options = {}) {
    // Can call other sources to get more data
    // Not just orders
    return await OrderService.getPersonalOrders(id, options.auth);
  }
}

module.exports = new AccountAction();
