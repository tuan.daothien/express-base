'use strict';

const BaseController = require('../base/base.controller');
const { AppError } = require('../utils/helpers/errors.helper');
const AccountAction = require('../actions/account.action');

class AccountController extends BaseController {
  constructor() {
    super();
    this.issuer = 'ACCOUNT_CONTROLLER';
  }

  async register(req, res, next) {
    try {
      const { username, password } = req.body;
      const [exist, result] = await AccountAction.register(username, password);

      if (exist) {
        throw new AppError('Username already used');
      }
      if (!result) {
        throw new AppError('Failed to register');
      }
      this.resSuccess(req, res)(result);
    } catch (e) {
      next(this.instanceError(e));
    }
  }

  async getProfile(req, res, next) {
    try {
      const { id } = req.params;
      const user = await AccountAction.getProfile(id);

      if (!user) {
        throw new AppError('User not found.', 404);
      }
      this.resSuccess(req, res)(user);
    } catch (e) {
      next(this.instanceError(e));
    }
  }

  async updateProfile(req, res, next) {
    try {
      const id = req.user.id.toString();
      const { body } = req;

      let updated = await AccountAction.updateProfile(id, body);

      if (!updated) {
        throw new AppError('Failed to update profile');
      }
      this.resSuccess(req, res)(updated);
    } catch (e) {
      next(this.instanceError(e));
    }
  }

  async socialize(req, res, next) {
    try {
      const actorId = req.user.id.toString();
      const id = req.params.id.toString();
      const { action } = req.query;

      const target = await AccountAction.socialize(id, actorId, action);
      if (!target) {
        throw new AppError(`Failed to ${action}`);
      }
      this.resSuccess(req, res)(target);
    } catch (e) {
      next(this.instanceError(e));
    }
  }

  async getFollowers(req, res, next) {
    try {
      const { id } = req.params;
      const { query } = req;

      const followers = await AccountAction.getFollowers(id, query);
      if (!followers) {
        throw new AppError('Failed to get followers');
      }
      this.resSuccess(req, res)(followers);
    } catch (e) {
      next(this.instanceError(e));
    }
  }

  async upload(req, res, next) {
    try {
      const { id } = req.params;
      const { files } = req;

      const result = await AccountAction.upload(id, files);
      if (!result) {
        throw new AppError('Failed to upload files');
      }
      this.resSuccess(req, res)(result);
    } catch (e) {
      next(this.instanceError(e));
    }
  }

  async login(req, res, next) {
    try {
      const { username, password } = req.body;

      const accessToken = await AccountAction.login(username, password);
      if (!accessToken) {
        throw new AppError('Username or password is incorrect');
      }
      this.resSuccess(req, res)({ access_token: accessToken });
    } catch (e) {
      next(this.instanceError(e));
    }
  }

  async getPersonalOrders(req, res, next) {
    try {
      const id = req.user.id.toString();
      const { auth } = req;

      const orders = await AccountAction.getPersonalOrders(id, { auth });
      this.resSuccess(req, res)(orders);
    } catch (e) {
      next(this.instanceError(e));
    }
  }
}

module.exports = new AccountController();
