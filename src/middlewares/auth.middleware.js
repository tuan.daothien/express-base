'use strict';

const jwt = require('jsonwebtoken');
const AppConfig = require('../app.config');
const { handleError, AppError } = require('../utils/helpers/errors.helper');

const authenticateJwt = (req, res, next) => {
  const authHeader = req.headers['authorization'];

  if (authHeader) {
    const token = authHeader.split(/\s/)[1];

    jwt.verify(token, AppConfig.APP_JWT_SECRET, (err, user) => {
      if (err) {
        return handleUnauthorized(res);
      }

      req.auth = authHeader;
      req.user = user;
      return next();
    });
  } else {
    return handleUnauthorized(res);
  }
};

const handleUnauthorized = (res) => {
  return handleError(new AppError('Unauthorized request', 401), res);
};

module.exports = { authenticateJwt };
