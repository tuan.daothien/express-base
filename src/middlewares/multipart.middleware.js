const multer = require('multer');
const { handleError, AppError } = require('../utils/helpers/errors.helper');
const storage = multer.memoryStorage();

const factory = {
  required: (value, files) => {
    return !value || files.length;
  },
  minCount: (value, files) => {
    return files.length >= value;
  },
  maxCount: (value, files) => {
    return files.length <= value;
  },
  mimeType: (value, files) => {
    const allowedMimeTypes = Array.isArray(value) ? value : [value];
    return !files.some((file) => !allowedMimeTypes.includes(file.mimetype));
  },
  maxSize: (value, files) => {
    return !files.some((file) => !(file.size <= value));
  }
};

const parse = (fields = []) => (req, res, next) => {
  let fieldsSimple = [];
  fieldsSimple = fields.map((field) => field.name);
  const upload = multer({ storage }).any();

  upload(req, res, () => {
    const errors = [];
    const files = {};
    req.files.forEach((file) => {
      const clientFieldName = file.fieldname;
      if (fieldsSimple.includes(clientFieldName)) {
        if (!files[clientFieldName]) {
          files[clientFieldName] = [file];
        } else {
          files[clientFieldName].push(file);
        }
      }
    });

    fields.forEach((field) => {
      const fieldName = field.name;
      const fieldValidations = field.validations;

      if (!fieldValidations) {
        return;
      }
      const targetFiles = files[fieldName] || [];
      Object.keys(fieldValidations).forEach((rule) => {
        const fieldRuleValue = fieldValidations[rule].value;
        if (!factory[rule](fieldRuleValue, targetFiles)) {
          const errorMsg =
            fieldValidations[rule].msg || `${fieldName}: ${rule}`;
          errors.push(errorMsg);
        }
      });
    });

    if (errors.length) {
      return handleError(new AppError(errors.toString(), 422), res);
    } else {
      req.files = files;
    }

    return next();
  });
};

module.exports = {
  parse
};
