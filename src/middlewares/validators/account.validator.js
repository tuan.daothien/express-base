'use strict';

const { body, check } = require('express-validator');

exports.validateRegister = () => {
  return [
    check('username')
      .exists()
      .withMessage('Username is requied')
      .isLength({ min: 6, max: 50 })
      .withMessage('Username must be between 6 and 50 characters'),
    check('password').exists().withMessage('Password is requied')
  ];
};

exports.validateLogin = () => {
  return [
    body('username').exists().withMessage('Username is required'),
    body('password').exists().withMessage('Password is required')
  ];
};
