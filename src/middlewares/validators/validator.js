'use strict';

const { validationResult } = require('express-validator');
const { handleError, AppError } = require('../../utils/helpers/errors.helper');

const validate = (validations) => async (req, res, next) => {
  for (let validation of validations) {
    const result = await validation.run(req);
    if (result.errors.length) break;
  }

  const errors = validationResult(req);
  if (errors.isEmpty()) {
    return next();
  }

  return handleError(new AppError(errors.array()[0].msg, 422), res);
};

module.exports = validate;
