'use strict';

const _isNullOrUndefined = (value) => {
  return value === null || value === undefined;
};
exports.isNullOrUndefined = _isNullOrUndefined;
