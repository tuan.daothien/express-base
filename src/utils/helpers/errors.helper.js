'use strict';

class AppError extends Error {
  constructor(message, code = 500) {
    super();
    this.code = code;
    this.message = message;
  }
}

const handleError = (err, res) => {
  let code = 500;
  let message = '';
  if (err instanceof AppError) {
    code = err.code;
    message = err.message;
  } else {
    code = 500;
    message = 'Server Error.';
  }
  res.status(code).json({
    success: false,
    message
  });
};

module.exports = {
  AppError,
  handleError
};
