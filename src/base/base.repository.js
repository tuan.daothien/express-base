'use strict';

const BaseClass = require('./base.class');

class BaseRepository extends BaseClass {
  constructor(model) {
    super();
    this.model = model;
  }

  async findById(id) {
    const result = await this.model.findOne({ where: { id } });
    return result;
  }

  async updateById(id, data) {
    const instance = await this.findById(id);
    const updated = await instance.update({ ...data });

    return updated;
  }

  async deleteById(id) {
    const count = await this.model.destroy({ where: { id } });
    return count;
  }
}

module.exports = BaseRepository;
