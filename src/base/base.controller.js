'use strict';

const BaseClass = require('./base.class');

class BaseController extends BaseClass {
  constructor() {
    super();
    this.issuer = 'BASE_CONTROLLER';
  }

  resSuccess(req, res) {
    return (data) => {
      return res.status(200).send({
        success: true,
        data
      });
    };
  }

  resPagination(req, res) {
    return (data) => {
      return res.status(200).send({
        success: true,
        data: data[0],
        pagination: data[1]
      });
    };
  }

  instanceError(e) {
    return {
      issuer: this.issuer,
      error: e
    };
  }
}

module.exports = BaseController;
