'use strict';

const axios = require('axios').default;
const BaseClass = require('./base.class');

class BaseHttpService extends BaseClass {
  constructor(serviceName) {
    super();
    this.serviceName = serviceName;
  }

  async get(uri, options = { params: {}, headers: {} }) {
    return await this.request('GET', uri, { ...options });
  }

  async post(uri, options = { params: {}, headers: {} }) {
    return await this.request('POST', uri, { ...options });
  }

  async put(uri, options = { params: {}, headers: {} }) {
    return await this.request('PUT', uri, { ...options });
  }

  async delete(uri, options = { params: {}, headers: {} }) {
    return await this.request('DELETE', uri, { ...options });
  }

  async request(method, uri, options = {}) {
    return await axios
      .request({
        method,
        baseURL: this.getBaseUrl(),
        url: uri,
        headers: options.headers,
        data: options.body,
        params: options.params
      })
      .then((response) => response.data)
      .catch((error) => {
        if (error.response) {
          throw error.response.data.message || 'Unknown error';
        } else {
          throw 'Failed to make request';
        }
      });
  }

  getBaseUrl() {
    // Use consult to get endpoint from this.serviceName
    return 'http://localhost:3001/api/orders';
  }

  getAuthHeader(value) {
    return {
      Authorization: value
    };
  }

  buildAuthHeaderOnly(value) {
    return {
      headers: this.getAuthHeader(value)
    };
  }
}

module.exports = BaseHttpService;
